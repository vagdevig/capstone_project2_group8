package com.greatlearning.hcl.capstone.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Property {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int propertyId;

	int ownerId;

	String propertyType;

	String propertyAddress;

	String pool;

	String garden;

	String parking;

	String imageurl;

	String propertyStatus;

	double price;

	public Property() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Property(int propertyId, int ownerId, String propertyType, String propertyAddress, String pool,
			String garden, String parking, String imageurl, String propertyStatus, double price) {
		super();
		this.propertyId = propertyId;
		this.ownerId = ownerId;
		this.propertyType = propertyType;
		this.propertyAddress = propertyAddress;
		this.pool = pool;
		this.garden = garden;
		this.parking = parking;
		this.imageurl = imageurl;
		this.propertyStatus = propertyStatus;
		this.price = price;
	}

	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public String getPool() {
		return pool;
	}

	public void setPool(String pool) {
		this.pool = pool;
	}

	public String getGarden() {
		return garden;
	}

	public void setGarden(String garden) {
		this.garden = garden;
	}

	public String getParking() {
		return parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getPropertyStatus() {
		return propertyStatus;
	}

	public void setPropertyStatus(String propertyStatus) {
		this.propertyStatus = propertyStatus;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}