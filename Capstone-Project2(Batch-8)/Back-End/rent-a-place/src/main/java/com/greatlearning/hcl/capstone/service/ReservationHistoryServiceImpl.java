package com.greatlearning.hcl.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.hcl.capstone.bean.ReservationHistory;
import com.greatlearning.hcl.capstone.dao.ReservationHistoryRepo;

@Service
public class ReservationHistoryServiceImpl implements IReservationHistoryService {

	@Autowired
	ReservationHistoryRepo reservationHistoryRepo;

	@Override
	public List<ReservationHistory> getOwnerReservationInfo(int ownerId) {

		return reservationHistoryRepo.findByOwnerId(ownerId);
	}

	@Override
	public List<ReservationHistory> getUserReservationInfo(int userId) {

		return reservationHistoryRepo.findByUserId(userId);
	}

}
