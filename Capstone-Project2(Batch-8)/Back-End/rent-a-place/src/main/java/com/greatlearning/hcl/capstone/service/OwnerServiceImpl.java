package com.greatlearning.hcl.capstone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.hcl.capstone.bean.Owner;
import com.greatlearning.hcl.capstone.dao.OwnerRepo;
import com.greatlearning.hcl.capstone.security.*;

@Service
public class OwnerServiceImpl implements IOwnerService {

	@Autowired
	OwnerRepo ownerRepo;

	@Autowired
	PasswordSecurity security;

	@Override
	public Owner save(Owner owner) {

		Owner registerOwner = new Owner();

		registerOwner.setName(owner.getName());
		registerOwner.setEmail(owner.getEmail());
		registerOwner.setTelephone(owner.getTelephone());

		String encodePassword = security.encodePassword(owner.getPassword());
		registerOwner.setPassword(encodePassword);

		return ownerRepo.save(registerOwner);
	}

	@Override
	public Owner login(String email, String password) {

		String encodePassword = security.encodePassword(password);

		return ownerRepo.findByEmailAndPassword(email, encodePassword);

	}

	@Override
	public Owner isPresent(String email) {

		Owner owner = ownerRepo.findByEmail(email);

		return owner;
	}
}
