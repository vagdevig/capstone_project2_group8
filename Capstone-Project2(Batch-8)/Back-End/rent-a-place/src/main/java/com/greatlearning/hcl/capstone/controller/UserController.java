package com.greatlearning.hcl.capstone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.hcl.capstone.bean.Property;
import com.greatlearning.hcl.capstone.bean.Reservation;
import com.greatlearning.hcl.capstone.bean.ReservationHistory;
import com.greatlearning.hcl.capstone.bean.ResponseMessage;
import com.greatlearning.hcl.capstone.bean.User;
import com.greatlearning.hcl.capstone.service.IPropertyService;
import com.greatlearning.hcl.capstone.service.IReservationHistoryService;
import com.greatlearning.hcl.capstone.service.IReservationService;
import com.greatlearning.hcl.capstone.service.IUserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	IUserService userService;

	@Autowired
	IPropertyService propertyService;

	@Autowired
	IReservationService reservationService;

	@Autowired
	IReservationHistoryService reservationHistoryService;

	@PostMapping("/saveUser")
	public ResponseEntity<ResponseMessage> saveUser(@RequestBody User user) {

		ResponseMessage rm = new ResponseMessage();

		if (userService.isPresent(user.getEmail()) == null) {
			userService.save(user);
			rm.setMessage("User is register...");
		} else {
			rm.setMessage("email should be unique");
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.CREATED);
	}

	@GetMapping("/loginUser")
	public ResponseEntity<User> loginUser(@RequestParam String email, String password) {
		return new ResponseEntity<User>(userService.login(email, password), HttpStatus.OK);
	}

	// Property Controller

	@GetMapping("/properties")
	public ResponseEntity<List<Property>> getProperties() {

		return new ResponseEntity<List<Property>>(propertyService.getProperties(), HttpStatus.OK);

	}

	@GetMapping("/getPropertyByAddress")
	public ResponseEntity<List<Property>> getPropertyByAddress(@RequestParam String propertyAddress) {

		return new ResponseEntity<List<Property>>(propertyService.getPropertyByAddress(propertyAddress), HttpStatus.OK);

	}

	// Reservation Controller

	@PostMapping("/reserve")
	public ResponseEntity<ResponseMessage> reserve(@RequestBody Reservation reservation) {

		ResponseMessage rm = new ResponseMessage();

		rm.setMessage(reservationService.reserve(reservation));

		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.CREATED);
	}

	@GetMapping("/getUserReservationInfo")
	public ResponseEntity<List<ReservationHistory>> getReservation(@RequestParam int userId) {

		return new ResponseEntity<List<ReservationHistory>>(reservationHistoryService.getUserReservationInfo(userId),
				HttpStatus.OK);

	}

}