package com.greatlearning.hcl.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.hcl.capstone.bean.Property;
import com.greatlearning.hcl.capstone.dao.PropertyRepo;

@Service
public class PropertyServiceImpl implements IPropertyService {

	@Autowired
	PropertyRepo propertyRepo;

	@Override
	public Property addProperty(Property property) {

		return propertyRepo.save(property);

	}

	@Override
	public List<Property> getOwnerProperty(int ownerId) {

		List<Property> ownerProperty = propertyRepo.findByOwnerId(ownerId);

		return ownerProperty;

	}

	@Override
	public List<Property> getPropertyByAddress(String propertyAddress) {

		List<Property> propertyByAddress = propertyRepo.findByPropertyAddress(propertyAddress);

		return propertyByAddress;
	}

	@Override
	public List<Property> getPropertyByAddressAndType(String propertyAddress, String propertyType) {

		List<Property> propertyByAddressAndType = propertyRepo.findByPropertyAddressAndPropertyType(propertyAddress,
				propertyType);

		return propertyByAddressAndType;
	}

	@Override
	public Property updateProperty(Property property) {

		return propertyRepo.save(property);
	}

	@Override
	public String deleteProperty(int propertyId) {

		propertyRepo.deleteById(propertyId);

		return "Property is deleted...";
	}

	@Override
	public List<Property> getProperties() {

		return propertyRepo.findAll();
	}

	@Override
	public String changePropertyStatus(int propertyId) {

		Property property = propertyRepo.findByPropertyId(propertyId);

		property.setPropertyStatus("Available");

		propertyRepo.save(property);

		return "Property Status Is Changed";
	}
}
