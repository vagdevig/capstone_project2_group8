package com.greatlearning.hcl.capstone;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentAPlaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentAPlaceApplication.class, args);
		System.err.println("welcome to rental bookings");
		System.err.println("server running on 9081");
	}

}
