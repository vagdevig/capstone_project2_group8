package com.greatlearning.hcl.capstone.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.hcl.capstone.bean.Owner;
@Repository
public interface OwnerRepo extends JpaRepository<Owner, Integer> {

	Owner findByEmailAndPassword(String email, String password);

	Owner findByEmail(String email);

}