package com.greatlearning.hcl.capstone.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.hcl.capstone.bean.Reservation;
@Repository
public interface ReservationRepo extends JpaRepository<Reservation, Integer> {

	Reservation findByReservationId(int reservationId);

	List<Reservation> findByOwnerId(int ownerId);

}
