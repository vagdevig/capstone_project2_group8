package com.greatlearning.hcl.capstone.service;

import com.greatlearning.hcl.capstone.bean.Owner;

public interface IOwnerService {

	public Owner save(Owner owner);

	public Owner isPresent(String email);

	public Owner login(String email, String password);

}
