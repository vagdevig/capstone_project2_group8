package com.greatlearning.hcl.capstone.service;

import java.util.List;

import com.greatlearning.hcl.capstone.bean.Reservation;

public interface IReservationService {

	String reserve(Reservation resevation);

	List<Reservation> getReservationRequest(int ownerId);

	String confirmReservation(int reservationId);

	String cancelReservation(int reservationId);
}
