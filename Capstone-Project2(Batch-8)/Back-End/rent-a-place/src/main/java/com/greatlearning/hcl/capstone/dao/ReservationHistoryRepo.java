package com.greatlearning.hcl.capstone.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.hcl.capstone.bean.ReservationHistory;
@Repository
public interface ReservationHistoryRepo extends JpaRepository<ReservationHistory, Integer> {

	List<ReservationHistory> findByOwnerId(int ownerId);

	ReservationHistory findByReservationHistoryId(int reservationHistoryId);

	List<ReservationHistory> findByUserId(int userId);

}
