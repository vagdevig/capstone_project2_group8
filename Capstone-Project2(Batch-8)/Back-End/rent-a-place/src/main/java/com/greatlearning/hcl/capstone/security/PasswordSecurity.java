package com.greatlearning.hcl.capstone.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.context.annotation.Configuration;

@Configuration
public class PasswordSecurity {

	public String encodePassword(String password) {

		String newPassword = password;
		String encodePassword = null;

		try {

			MessageDigest m = MessageDigest.getInstance("MD5");

			m.update(newPassword.getBytes());

			byte[] bytes = m.digest();

			StringBuilder s = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			encodePassword = s.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return encodePassword;

	}
}