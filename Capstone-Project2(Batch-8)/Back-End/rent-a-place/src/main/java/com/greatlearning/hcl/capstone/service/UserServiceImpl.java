package com.greatlearning.hcl.capstone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.hcl.capstone.bean.User;
import com.greatlearning.hcl.capstone.dao.UserRepo;
import com.greatlearning.hcl.capstone.security.PasswordSecurity;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	UserRepo userRepo;

	@Autowired
	PasswordSecurity security;

	@Override
	public User save(User user) {

		User registerUser = new User();

		registerUser.setFirstname(user.getFirstname());
		registerUser.setLastname(user.getLastname());
		registerUser.setEmail(user.getEmail());
		registerUser.setTelephone(user.getTelephone());

		String encodePassword = security.encodePassword(user.getPassword());
		registerUser.setPassword(encodePassword);

		registerUser.setAddress(user.getAddress());

		return userRepo.save(registerUser);
	}

	@Override
	public User login(String email, String password) {

		String encodePassword = security.encodePassword(password);

		User user = userRepo.findByEmailAndPassword(email, encodePassword);

		return user;
	}

	@Override
	public User isPresent(String email) {

		User user = userRepo.findByEmail(email);

		return user;
	}
}
