package com.greatlearning.hcl.capstone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.hcl.capstone.bean.*;
import com.greatlearning.hcl.capstone.dao.PropertyRepo;
import com.greatlearning.hcl.capstone.dao.ReservationHistoryRepo;
import com.greatlearning.hcl.capstone.dao.ReservationRepo;

@Service
public class ReservationServiceImpl implements IReservationService {

	@Autowired
	ReservationRepo reservationRepo;

	@Autowired
	ReservationHistoryRepo reservationHistoryRepo;

	@Autowired
	PropertyRepo propertyRepo;

	@Override
	public String reserve(Reservation reservation) {

		reservationRepo.save(reservation);

		return "Reserve request sent successfully";
	}

	@Override
	public List<Reservation> getReservationRequest(int ownerId) {

		return reservationRepo.findByOwnerId(ownerId);

	}

	@Override
	public String confirmReservation(int reservationId) {

		Reservation reservation = reservationRepo.findByReservationId(reservationId);

		ReservationHistory reservationHistory = new ReservationHistory();

		reservationHistory.setReservationId(reservationId);
		reservationHistory.setPropertyId(reservation.getPropertyId());
		reservationHistory.setOwnerId(reservation.getOwnerId());
		reservationHistory.setUserId(reservation.getUserId());
		reservationHistory.setCheckIn(reservation.getCheckIn());
		reservationHistory.setCheckOut(reservation.getCheckOut());
		reservationHistory.setPrice(reservation.getPrice());
		reservationHistory.setReservationStatus("Confirm");

		Property updatePropertyStatus = propertyRepo.findByPropertyId(reservation.getPropertyId());
		updatePropertyStatus.setPropertyStatus("Reserved");

		propertyRepo.save(updatePropertyStatus);

		reservationHistoryRepo.save(reservationHistory);

		reservationRepo.deleteById(reservationId);

		return "Property is Reserved...";
	}

	@Override
	public String cancelReservation(int reservationId) {

		Reservation reservation = reservationRepo.findByReservationId(reservationId);

		ReservationHistory reservationHistory = new ReservationHistory();

		reservationHistory.setReservationId(reservationId);
		reservationHistory.setPropertyId(reservation.getPropertyId());
		reservationHistory.setOwnerId(reservation.getOwnerId());
		reservationHistory.setUserId(reservation.getUserId());
		reservationHistory.setCheckIn(reservation.getCheckIn());
		reservationHistory.setCheckOut(reservation.getCheckOut());
		reservationHistory.setPrice(reservation.getPrice());
		reservationHistory.setReservationStatus("Cancel");

		Property updatePropertyStatus = propertyRepo.findByPropertyId(reservation.getPropertyId());
		updatePropertyStatus.setPropertyStatus("Available");
		propertyRepo.save(updatePropertyStatus);

		reservationHistoryRepo.save(reservationHistory);

		reservationRepo.deleteById(reservationId);

		return "Property Reservation is Cancle...";
	}

}
