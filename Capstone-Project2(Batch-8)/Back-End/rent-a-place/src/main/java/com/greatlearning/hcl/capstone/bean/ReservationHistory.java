package com.greatlearning.hcl.capstone.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ReservationHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int reservationHistoryId;

	private int reservationId;

	private String checkIn;
	private String checkOut;
	private double price;
	private String reservationStatus;

	int propertyId;
	int ownerId;
	int userId;

	public ReservationHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReservationHistory(int reservationHistoryId, int reservationId, String checkIn, String checkOut,
			double price, String reservationStatus, int propertyId, int ownerId, int userId) {
		super();
		this.reservationHistoryId = reservationHistoryId;
		this.reservationId = reservationId;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.price = price;
		this.reservationStatus = reservationStatus;
		this.propertyId = propertyId;
		this.ownerId = ownerId;
		this.userId = userId;
	}

	public int getReservationHistoryId() {
		return reservationHistoryId;
	}

	public void setReservationHistoryId(int reservationHistoryId) {
		this.reservationHistoryId = reservationHistoryId;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
