package com.greatlearning.hcl.capstone.service;

import java.util.List;

import com.greatlearning.hcl.capstone.bean.Property;

public interface IPropertyService {

	public Property addProperty(Property property);

	public List<Property> getOwnerProperty(int owner_id);

	public List<Property> getPropertyByAddress(String property_address);

	public Property updateProperty(Property property);

	public String deleteProperty(int property_id);

	public List<Property> getProperties();

	public List<Property> getPropertyByAddressAndType(String propertyAddress, String propertyType);

	public String changePropertyStatus(int propertyId);

}
