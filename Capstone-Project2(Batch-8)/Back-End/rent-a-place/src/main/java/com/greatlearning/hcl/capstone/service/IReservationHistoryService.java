package com.greatlearning.hcl.capstone.service;

import java.util.List;

import com.greatlearning.hcl.capstone.bean.ReservationHistory;

public interface IReservationHistoryService {

	List<ReservationHistory> getOwnerReservationInfo(int ownerId);

	List<ReservationHistory> getUserReservationInfo(int userId);

}
