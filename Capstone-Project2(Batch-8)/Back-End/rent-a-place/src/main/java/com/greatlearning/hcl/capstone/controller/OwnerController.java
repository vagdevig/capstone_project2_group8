package com.greatlearning.hcl.capstone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.hcl.capstone.bean.Owner;
import com.greatlearning.hcl.capstone.bean.Property;
import com.greatlearning.hcl.capstone.bean.Reservation;
import com.greatlearning.hcl.capstone.bean.ReservationHistory;
import com.greatlearning.hcl.capstone.bean.ResponseMessage;
import com.greatlearning.hcl.capstone.service.IOwnerService;
import com.greatlearning.hcl.capstone.service.IPropertyService;
import com.greatlearning.hcl.capstone.service.IReservationHistoryService;
import com.greatlearning.hcl.capstone.service.IReservationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class OwnerController {

	@Autowired
	IOwnerService ownerService;

	@Autowired
	IPropertyService propertyService;

	@Autowired
	IReservationService reservationService;

	@Autowired
	IReservationHistoryService reservationHistoryService;

	@PostMapping("/saveOwner")
	public ResponseEntity<ResponseMessage> saveOwner(@RequestBody Owner owner) {

		ResponseMessage rm = new ResponseMessage();

		if (ownerService.isPresent(owner.getEmail()) == null) {
			ownerService.save(owner);
			rm.setMessage("Owner is register...");

		}

		else {
			rm.setMessage("Email should be unique...");
		}
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.CREATED);
	}

	@GetMapping("/loginOwner")
	public ResponseEntity<Owner> loginOwner(@RequestParam String email, String password) {

		return new ResponseEntity<Owner>(ownerService.login(email, password), HttpStatus.OK);

	}

	// Property Controller

	@PostMapping("/addProperty")
	public ResponseEntity<Property> addProperty(@RequestBody Property property) {

		return new ResponseEntity<Property>(propertyService.addProperty(property), HttpStatus.CREATED);

	}

	@GetMapping("/getOwnerProperty")
	public ResponseEntity<List<Property>> getOwnerProperty(@RequestParam int ownerId) {

		return new ResponseEntity<List<Property>>(propertyService.getOwnerProperty(ownerId), HttpStatus.OK);

	}

	@PutMapping("/updateProperty")
	public ResponseEntity<Property> updateProperty(@RequestBody Property property) {

		return new ResponseEntity<Property>(propertyService.updateProperty(property), HttpStatus.CREATED);
	}

	@DeleteMapping("/deleteProperty")
	public ResponseEntity<ResponseMessage> deleteProperty(@RequestParam int propertyId) {

		ResponseMessage rm = new ResponseMessage();

		rm.setMessage(propertyService.deleteProperty(propertyId));

		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@GetMapping("/changePropertyStatus")
	public ResponseEntity<ResponseMessage> changePropertyStatus(@RequestParam int propertyId) {

		ResponseMessage rm = new ResponseMessage();

		rm.setMessage(propertyService.changePropertyStatus(propertyId));

		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	// Reservation Controller

	@GetMapping("/getReservationRequest")
	public ResponseEntity<List<Reservation>> getReservationRequest(@RequestParam int ownerId) {
		return new ResponseEntity<List<Reservation>>(reservationService.getReservationRequest(ownerId), HttpStatus.OK);
	}

	@GetMapping("/getOwnerReservationInfo")
	public ResponseEntity<List<ReservationHistory>> getReservationInfo(@RequestParam int ownerId) {

		return new ResponseEntity<List<ReservationHistory>>(reservationHistoryService.getOwnerReservationInfo(ownerId),
				HttpStatus.OK);

	}

	@GetMapping("/confirmReservation")
	public ResponseEntity<ResponseMessage> confirmReservation(@RequestParam int reservationId) {

		ResponseMessage rm = new ResponseMessage();

		rm.setMessage(reservationService.confirmReservation(reservationId));

		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@GetMapping("/cancelReservation")
	public ResponseEntity<ResponseMessage> cancelReservation(@RequestParam int reservationId) {

		ResponseMessage rm = new ResponseMessage();

		rm.setMessage(reservationService.cancelReservation(reservationId));

		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
}
