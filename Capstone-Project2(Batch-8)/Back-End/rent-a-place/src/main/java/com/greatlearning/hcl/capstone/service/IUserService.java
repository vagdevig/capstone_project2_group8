package com.greatlearning.hcl.capstone.service;

import com.greatlearning.hcl.capstone.bean.User;

public interface IUserService {

	public User save(User user);

	public User isPresent(String email);

	public User login(String email, String password);

}
