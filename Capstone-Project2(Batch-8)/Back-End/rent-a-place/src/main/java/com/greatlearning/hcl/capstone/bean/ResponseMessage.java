package com.greatlearning.hcl.capstone.bean;

public class ResponseMessage {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
