package com.greatlearning.hcl.capstone.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.hcl.capstone.bean.Property;
@Repository
public interface PropertyRepo extends JpaRepository<Property, Integer> {

	List<Property> findByOwnerId(int owner_id);

	List<Property> findByPropertyAddress(String name);

	List<Property> findByPropertyAddressAndPropertyType(String propertyAddress, String propertyType);

	Property findByPropertyId(int propertyId);

}
