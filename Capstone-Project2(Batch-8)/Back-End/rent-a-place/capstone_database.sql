create database rentaplace_capstone;

use rentaplace_capstone;

create table owner(owner_id int primary key auto_increment,email varchar(255),name varchar(255), password varchar(255),telephone varchar(255));

create table user(user_id int primary key auto_increment,address varchar(255),email varchar(255),firstname varchar(255), lastname varchar(255), password varchar(255), telephone varchar(255));

create table property(property_id int primary key auto_increment,garden varchar(255), imageurl varchar(9600), owner_id int not null, parking varchar(255), pool varchar(255), price double not null,property_address varchar(255),property_status varchar(255), property_type varchar(255));

create table reservation(reservation_id int primary key auto_increment,check_in varchar(255),check_out varchar(255),owner_id int not null,price double not null,property_id int not null,reservation_status varchar(255),user_id int not null);

create table reservation_history(reservation_history_id int primary key,check_in varchar(255),check_out varchar(255),owner_id int not null,price double not null,property_id int not null,reservation_id int not null,reservation_status varchar(255),user_id int not null);